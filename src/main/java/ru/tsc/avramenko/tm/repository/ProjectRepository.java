package ru.tsc.avramenko.tm.repository;

import ru.tsc.avramenko.tm.api.repository.IProjectRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Project;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @Override
    public List<Project> findAll(final String userId, final Comparator<Project> comparator) {
        final List<Project> projects = findAll(userId);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public boolean existsById(String userId, final String id) {
        return findById(userId, id) != null;
    }

    @Override
    public Project findByName(final String userId, final String name) {
        for (Project project : list) {
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final String userId, final int index) {
        final List<Project> projects = findAll(userId);
        return projects.get(index);
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final int index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        list.remove(project);
        return project;
    }

    @Override
    public Project startById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project startByIndex(final String userId, final int index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
        return project;
    }

    @Override
    public Project finishById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project finishByIndex(final String userId, final int index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        project.setFinishDate(new Date());
        return project;
    }

    @Override
    public Project changeStatusById(final String userId, String id, Status status) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByName(final String userId, String name, Status status) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final String userId, int index, Status status) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}