package ru.tsc.avramenko.tm.repository;

import ru.tsc.avramenko.tm.api.repository.IOwnerRepository;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.model.AbstractOwnerEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E>
        implements IOwnerRepository<E> {

    @Override
    public E add(final String userId, final E entity) {
        entity.setUserId(userId);
        list.add(entity);
        return entity;
    }

    @Override
    public void remove(final String userId, final E entity) {
        final List<E> listEnt = findAll(userId);
        list.remove(listEnt);
    }

    @Override
    public List<E> findAll(final String userId) {
        final List<E> entities = new ArrayList<>();
        for (final E entity : list) {
            if (userId.equals(entity.getUserId()))
                entities.add(entity);
        }
        return entities;
    }

    @Override
    public void clear(final String userId) {
        final List<E> listEnt = findAll(userId);
        for (E entity : listEnt) {
            this.list.remove(entity);
        }
    }

    @Override
    public E findById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        for (final E entity : list) {
            if (entity == null) continue;
            if (!userId.equals(entity.getUserId())) continue;
            if (id.equals(entity.getId())) return entity;
        }
        return null;
    }

    @Override
    public E removeById(final String userId, final String id) {
        final E entity = findById(userId, id);
        if (entity == null) return null;
        remove(userId, entity);
        return entity;
    }

}