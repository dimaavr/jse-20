package ru.tsc.avramenko.tm.repository;

import ru.tsc.avramenko.tm.api.repository.ITaskRepository;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Task;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAll(final String userId, final Comparator<Task> comparator) {
        final List<Task> tasks = findAll(userId);
        tasks.sort(comparator);
        return tasks;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findById(userId, id) != null;
    }

    @Override
    public Task findByName(final String userId, final String name) {
        for (Task task : list) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final String userId, int index) {
        final List<Task> tasks = findAll(userId);
        return tasks.get(index);
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        list.remove(task);
        return task;
    }

    @Override
    public Task startById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task startByIndex(final String userId, final int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        return task;
    }

    @Override
    public Task finishById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task finishByIndex(final String userId, final int index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        return task;
    }

    @Override
    public Task changeStatusById(final String userId, final String id, final Status status) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String userId, final String name, final Status status) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, final int index, final Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, String id) {
        final List<Task> taskList = new ArrayList<>();
        for (Task task : list) {
            if (id.equals(task.getProjectId()) && userId.equals(task.getUserId())) taskList.add(task);
        }
        if (taskList.size() == 0) return null;
        return taskList;
    }

    @Override
    public Task bindTaskToProjectById(final String userId, String projectId, String taskId) {
        final Task task = findById(userId, taskId);
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public Task unbindTaskById(final String userId, String id) {
        final Task task = findById(userId, id);
        task.setProjectId(null);
        return task;
    }

    @Override
    public void unbindAllTaskByProjectId(final String userId, String id) {
        List<Task> taskList = findAllTaskByProjectId(userId, id);
        for (Task task : taskList) {
            if (id.equals(task.getProjectId())) task.setProjectId(null);
        }
    }

}