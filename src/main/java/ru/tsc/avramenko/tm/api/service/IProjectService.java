package ru.tsc.avramenko.tm.api.service;

import ru.tsc.avramenko.tm.api.IService;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Project;
import java.util.Comparator;
import java.util.List;

public interface IProjectService extends IOwnerService<Project> {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    Project changeStatusById(String userId, String id, Status status);

    Project changeStatusByName(String userId, String name, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);

    List<Project> findAll(String userId, Comparator<Project> comparator);

    Project findByName(String userId, String name);

    Project findByIndex(String userId, Integer index);

    Project updateByIndex(String userId, Integer index, String name, String description);

    Project updateById(String userId, String id, String name, String description);

    Project startById(String userId, String id);

    Project startByName(String userId, String name);

    Project startByIndex(String userId, Integer index);

    Project finishById(String userId, String id);

    Project finishByName(String userId, String name);

    Project finishByIndex(String userId, Integer index);

}