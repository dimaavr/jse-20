package ru.tsc.avramenko.tm.api.service;

import ru.tsc.avramenko.tm.api.IService;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IOwnerService<Task> {

    void create(String userId, String name);

    void create(String userId, String name, String description);

    Task changeStatusById(String userId, String id, Status status);

    Task changeStatusByName(String userId, String name, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    List<Task> findAll(String userId, Comparator<Task> comparator);

    Task findByName(String userId, String name);

    Task findByIndex(String userId, Integer index);

    Task removeByName(String userId, String name);

    Task removeByIndex(String userId, Integer index);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task updateById(String userId, String id, String name, String description);

    Task startById(String userId, String id);

    Task startByName(String userId, String name);

    Task startByIndex(String userId, Integer index);

    Task finishById(String userId, String id);

    Task finishByName(String userId, String name);

    Task finishByIndex(String userId, Integer index);

}