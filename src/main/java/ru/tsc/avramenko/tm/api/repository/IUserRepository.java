package ru.tsc.avramenko.tm.api.repository;

import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(String login);

    User findByEmail(String email);

    User removeUserById(String id);

    User removeUserByLogin(String login);

}