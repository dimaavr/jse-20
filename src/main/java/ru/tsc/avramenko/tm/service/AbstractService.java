package ru.tsc.avramenko.tm.service;

import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.api.IService;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.model.AbstractEntity;

import java.util.List;

public abstract class AbstractService <E extends AbstractEntity> implements IService<E> {

    protected final IRepository<E> repository;

    public AbstractService(IRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    public void add(final E entity) {
        if (entity == null) return;
        repository.add(entity);
    }

    @Override
    public void remove(final E entity) {
        if (entity == null) return;
        repository.remove(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    public E findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    public E removeById(final String id) {
        if (id == null) throw new EmptyIdException();
        return repository.removeById(id);
    }

}